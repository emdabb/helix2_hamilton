// GLSL shader autogenerated by cg2glsl.py.
#if defined(VERTEX)

#if __VERSION__ >= 130
#define COMPAT_VARYING out
#define COMPAT_ATTRIBUTE in
#define COMPAT_TEXTURE texture
#else
#define COMPAT_VARYING varying
#define COMPAT_ATTRIBUTE attribute
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif
struct InputVS {
    vec4 _Position1;
    vec2 _TexCoord1;
};
struct OutputVS {
    vec4 _Position;
    vec2 _TexCoord;
};
struct OutputPS {
    vec4 _Color;
};
OutputVS _ret_0;
vec4 _r0009;
COMPAT_ATTRIBUTE vec4 VertexCoord;
COMPAT_ATTRIBUTE vec4 TexCoord0;
COMPAT_VARYING vec4 TEX0;
uniform mat4 _WorldViewProj;

void main()
{
    _r0009.x = dot(_WorldViewProj[0], VertexCoord);
    _r0009.y = dot(_WorldViewProj[1], VertexCoord);
    _r0009.z = dot(_WorldViewProj[2], VertexCoord);
    _r0009.w = dot(_WorldViewProj[3], VertexCoord);
    _ret_0._Position = _r0009;
    _ret_0._TexCoord = TexCoord0.xy;
    gl_Position = _r0009;
    TEX0.xy = TexCoord0.xy;
    TEX0.xy = _ret_0._TexCoord;
    return;
}
#elif defined(FRAGMENT)

#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif
struct InputVS {
    vec2 _TexCoord1;
};
struct OutputVS {
    vec2 _TexCoord;
};
struct OutputPS {
    vec4 _Color;
};
vec2 _c0006;
vec2 _c0010;
COMPAT_VARYING vec4 TEX0;
uniform sampler2D BaseSampler;
uniform vec2 _gDistance;
uniform vec2 _gResolution;
#if 0
// Given a vec2 in [-1,+1], generate a texture coord in [0,+1]
vec2 Distort(vec2 tc)
{
    vec2 p = tc * 2.0 - 1.0;
    float theta  = atan(p.y, p.x);
    float radius = length(p);
    radius = pow(radius, 1.5);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
    return 0.5 * (p + 1.0);
}
#else
vec2 Distort(vec2 p) {
    float k = -1.0;
    float kcube = 0.5;

    vec2 p2 = p - vec2(0.5, 0.5);
    float r2 = dot(p2, p2);
    float f =0.0;
    if(kcube == 0.0) {
        f = 1.0 + r2 * k;
    } else {
        f = 1.0 + r2 * (k + kcube * sqrt(r2));
    }

    return f * p2 + vec2(0.5, 0.5);
}
#endif


void main()
{
    vec2 _value;
    vec4 _c0;
    vec4 _c1;
    vec4 _c2;
    OutputPS _Out;
    vec2 tc = (gl_FragCoord.xy / _gResolution.xy);
    vec2 uv = tc; //Distort(tc);

    //_value = _gDistance * (0.5 * _gResolution.xy - gl_FragCoord.xy) / _gResolution.xy;
    _value = _gDistance * (0.5 * _gResolution.xy) / _gResolution.xy;
    _c0006 = uv - _value/_gResolution.x;
    _c0010 = uv + _value/_gResolution.y;
    vec2 _c0011 = uv - _value/_gResolution.y;

    _c0 = COMPAT_TEXTURE(BaseSampler, _c0006);
    _c1 = COMPAT_TEXTURE(BaseSampler, _c0011);
    _c2 = COMPAT_TEXTURE(BaseSampler, _c0010);

    _Out._Color = vec4(_c0.x, _c1.y, _c2.z, _c0.w + _c1.w + _c2.w);
    //_Out._Color.xyz *= _Out._Color.w;
    FragColor = _Out._Color;
    return;
}
#endif
