//
//  AppDelegate.h
//  hagar_ios
//
//  Created by emiel Dam on 6/17/15.
//  Copyright (c) 2015 TFjoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <helix/base/platform/ios/IOSApplication.h>

typedef helix::base::impl::IOSApplication PlatformApplication;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UIWindow* window;
    PlatformApplication* app;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;


@end

