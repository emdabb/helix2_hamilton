//
//  AppDelegate.m
//  hagar_ios
//
//  Created by emiel Dam on 6/17/15.
//  Copyright (c) 2015 TFjoy. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"
#include <helix/core/DebugLog.h>

using namespace helix::base;
using namespace helix::core;

HXAPI HXEXPORT IApplet* new_Game(IApplication*);

@implementation AppDelegate

@synthesize window;

- (void) startApplication:(id)mainWindow {
    DEBUG_METHOD();
    app = new PlatformApplication;
    app->setWindow((__bridge void*)mainWindow);
    app->setApplet(new_Game(app));
    app->create();
    app->start();
    

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DEBUG_METHOD();
        
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    if(NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
        CGFloat tmp = screenBounds.size.width;
        screenBounds.size.width = screenBounds.size.height;
        screenBounds.size.height= tmp;
    }

    window = [[UIWindow alloc] initWithFrame:screenBounds];
    

    window.center = CGPointMake(screenBounds.size.height / 2.0f, screenBounds.size.width / 2.0f);
    [window setTransform:CGAffineTransformMakeRotation(M_PI / 2.0f)];

    [self startApplication:window];
    [window makeKeyAndVisible];
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    DEBUG_METHOD();
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidEnterForeground:(UIApplication *)application {
    DEBUG_METHOD();
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void) applicationDidBecomeActive:(UIApplication *)application {

}

@end
