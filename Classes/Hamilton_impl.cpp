/*
 * Hamilton_impl.cpp
 *
 *  Created on: Jul 21, 2015
 *      Author: miel
 */

#include <helix/Types.h>
#include <helix/core/Matrix.h>
#include <helix/engine/GameArt.h>
#include <helix/engine/GameServices.h>
#include <helix/engine/GameScreen.h>
#include <helix/engine/gui/Gui.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/ClearOptions.h>
#include <helix/gfx/Color.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/gfx/RenderState.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/IGraphicsContext.h>
#include <Classes/Game/Game.h>
#include <Classes/Game/Input.h>
#include <Classes/Game/BloomEffect.h>

using namespace helix::audio;
using namespace helix::core;
using namespace helix::base;
using namespace helix::engine;
using namespace helix::gfx;

HXAPI GameScreen* new_MessageBox(const char* str);
HXAPI GameScreen* new_GameplayScreen();



class Hamilton : public Game {
	hxApplication* mApplication;
	GameScreenManager mScreenManager;
	StateMachine<Game> mStateMachine;
	BloomEffect* mEffect;
	Texture2D* mRenderTarget;
public:
	Hamilton(hxApplication* app)
	: mApplication(app)
	, mEffect(NULL)
	, mRenderTarget(NULL)
	{
		mStateMachine.setOwner(this);

		mApplication->OnMouseMoved    += event(Gui::getInstancePtr(), &Gui::onMouseMoved);
		mApplication->OnMousePressed  += event(Gui::getInstancePtr(), &Gui::onMousePressed);
		mApplication->OnMouseReleased += event(Gui::getInstancePtr(), &Gui::onMouseReleased);

		mApplication->OnMouseMoved    += event(&Input::onMouseMoved);
		mApplication->OnMousePressed  += event(&Input::onMousePressed);
		mApplication->OnMouseReleased += event(&Input::onMouseReleased);
	}

	virtual ~Hamilton() {

	}
    virtual void create() {


    	mApplication->getGraphicsContext()->acquire();

    	GameServices::getInstance().setProvider(mApplication);

    	GameArt::getInstance().loadFonts("fonts.json", 24);
    	GameArt::getInstance().loadShaders("shaders.json");
    	GameArt::getInstance().loadTextures("textures.json");
    	GameArt::getInstance().loadAudio("audiolib.json");

    	mEffect 		= new BloomEffect(mApplication->getGraphicsDevice(), 1024, 768);
    	mRenderTarget 	= new Texture2D(mApplication->getGraphicsDevice(), 1024, 768, PixelFormat::Color, false);

    	mApplication->getGraphicsContext()->release();

    	mScreenManager.addScreen(new_GameplayScreen());

    	GameServices::getInstance().getAudioDevice()->play(GameArt::getInstance().getAudioById(0), 1.0f, 1.0f);
    }
    virtual void destroy() {

    }
    virtual void update(int dt) {
    	mScreenManager.update(dt);
    	GameServices::getInstance().getAudioDevice()->update();
    }
    virtual void draw() {
    	SpriteBatch* sprites = GameServices::getInstance().getSpriteBatch();
    	IGraphicsDevice* device = GameServices::getInstance().getGraphicsDevice();

        //Viewport vp = device->getViewport();

    	if(device->startRendering()) {
    		device->setRenderTarget(mRenderTarget, 0);
    		device->clear(ColorPreset::TransparentBlack, ClearOptions::ClearAll, 0, 1.f);
    		sprites->begin(0, BlendState::AlphaBlend, Matrix::Identity, GameArt::getInstance().getShaderProgram("spriteBatchDefault"));
    		mScreenManager.draw(16);
    		sprites->end();
    		device->resolveRenderTarget(0);

    		device->clear(ColorPreset::Black, ClearOptions::ClearAll, 0, 1.f);
    		mEffect->draw(mRenderTarget, NULL);
    		device->stopRendering();
    	}
    }

    virtual GameScreenManager& getScreenManager() {
    	return mScreenManager;
    }

    virtual StateMachine<Game>& getStateMachine() {
    	return mStateMachine;
    }

};

HXAPI HXEXPORT hxApplet* new_Game(hxApplication* app) {
	return new Hamilton(app);
}
