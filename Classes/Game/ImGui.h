#ifndef JNI_GAME_GUI_GUI_H_
#define JNI_GAME_GUI_GUI_H_

#include <cstdint>
#include <helix/base/IApplication.h>
#include <helix/core/Rectangle.h>
#include <helix/core/Vector2.h>
#include <helix/core/Matrix.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/SpriteFont.h>

#define UID     (__COUNTER__ + 1024)

static struct gui_state {
    short mousex;
    short mousey;
    char mousedown;

    int hot;
    int active;

    int kbditem;
    int keyentered;
    int keymod;
    int keychar;

    char last;
    int layout;
    int spacing;
} uistate = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

struct ImGui {

	typedef void(*DrawCallback)(int id, const hxRectangle&);

    static void  onKeyPress(const hxKeyEventArgs& args);
    static void  setAlpha(char a);
    static bool  mouseHit(const hxRectangle& rect);
    static void  beginGui(const hxMatrix&);
    static void  endGui();
    static void  cursor(hxTexture2D* texid, int x, int y, hxRectangle* = NULL);
    static bool  button(int id, const char* text, hxSpriteFont* fontid, const hxRectangle& rect, hxTexture2D* tex, unsigned char alpha, const hxRectangle* = NULL);
    static void  label(int id, const hxVector2& pos, const char* text, hxSpriteFont* fontid, unsigned char alpha, bool center, hxRectangle* = NULL);
    static float scrollbar(int id, const hxRectangle& rectangle, hxTexture2D* scrollbarTextureId, hxTexture2D* gripTextureId, float max, float value, bool horizontal, hxRectangle* = NULL);
    static void  canvas(int id, const hxRectangle& rect, hxTexture2D* texid, bool flip, hxRectangle* = NULL);
    static bool  textInput(int id, const hxRectangle& rect, hxTexture2D* tex, char* buffer, hxSpriteFont* fontid, unsigned char alpha, const hxRectangle* = NULL);

    static void  row(int spacing);
    static void  col(int spacing);

};
#endif /* JNI_GAME_GUI_GUI_H_ */

