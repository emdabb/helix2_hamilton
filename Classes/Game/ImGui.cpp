#include "ImGui.h"
#include "Input.h"
#include <helix/gfx/Color.h>
#include <helix/engine/GameArt.h>
#include <helix/engine/GameServices.h>

using namespace helix::base;
using namespace helix::engine;
using namespace helix::gfx;
using namespace helix::core;

void ImGui::onKeyPress(const KeyEventArgs& args) {
	uistate.keychar = args.KeyId;
}

void ImGui::setAlpha(char a) {
	//mDrawColor.A = mActiveColor.A = mHotColor.A = a;
}

bool ImGui::mouseHit(const hxRectangle& rect) {
	return Rectangle::contains(rect, (int) (Input::MousePosition[0].X + 0.5f),
			(int) (Input::MousePosition[0].Y + 0.5f));
}

void ImGui::beginGui(const hxMatrix& a) {
	uistate.hot = 0;
	uistate.active = 0;
	uistate.kbditem = 0;
	uistate.keychar = Input::KeyPressed;
	//batch->begin(0, BlendState::AlphaBlend);
	GameServices::getInstance().getSpriteBatch()->begin(0,
			BlendState::AlphaBlend, a,
			GameArt::getInstance().getShaderProgram("spriteBatchDefault"));
}

void ImGui::endGui() {
	GameServices::getInstance().getSpriteBatch()->end();

	if (!Input::MouseButtonDown[0]) { //LeftMouseDown) {
		uistate.active = 0;
	}
}

void ImGui::cursor(hxTexture2D* texid, int x, int y, hxRectangle*) {
	GameServices::getInstance().getSpriteBatch()->draw(texid, (Vector2 ) {
							(float) x, (float) y }, ColorPreset::White);
}

bool ImGui::button(int id, const char* text, hxSpriteFont* fontid, const Rectangle& rect, hxTexture2D* texid, unsigned char alpha, const hxRectangle* src) {
	bool mouseWasPressed = Input::MouseButtonDown[0]; //LeftMouseDown;
	if (mouseHit(rect)) {
		uistate.hot = id;
		// Here?
		Input::MouseButtonDown[0] = false; //LeftMouseDown = false;
		if (uistate.active == 0 && mouseWasPressed) {
			uistate.active = id;

		}
	}
	Color drawColor;
	if (uistate.hot == id) {
		if (uistate.active == id) {
			drawColor = ColorPreset::Yellow; //mActiveColor;
		} else {
			drawColor = ColorPreset::White; //mHotColor;
		}
	} else {
		drawColor = ColorPreset::LightGray; //mDrawColor;
	}
	drawColor.A = alpha;
	//SpriteFont* font = GameArt::getInstance().getFontById(fontid);


	SpriteBatch* sb = GameServices::getInstance().getSpriteBatch();

	sb->draw(texid, rect, (hxRectangle*)src, drawColor);

	if(strlen(text)) {
		Vector2 size = fontid->measureString(text);
		sb->drawString(fontid, text, (Vector2) { rect.X + rect.W * 0.5f - size.X * 0.5f, rect.Y + rect.H * 0.5f + size.Y * 0.5f }, ColorPreset::Black);
	}

	return mouseWasPressed && id == uistate.active && id == uistate.hot;
}

void ImGui::label(int id, const Vector2& pos, const char* text, hxSpriteFont* font, unsigned char alpha, bool center, hxRectangle*) {
	//SpriteFont* font = GameArt::getInstance().getFontById(fontid);
	Vector2 p = pos;
	if (center) {

		Vector2 h = font->measureString(text);
		h *= 0.5f;
		p.X -= h.X;
	}
	Color dc = ColorPreset::LimeGreen;
	dc.A = alpha;
	SpriteBatch* sb = GameServices::getInstance().getSpriteBatch();
	sb->drawString(font, text, p, dc);
}

float ImGui::scrollbar(int id, const Rectangle& rectangle, hxTexture2D* scrollbarTexture, hxTexture2D* gripTexture, float max, float value, bool horizontal, hxRectangle*) {

	//No matter the input, value should be at least 0 and at most max
	value = std::min(std::max(value, 0.f), max);
	//Determine if we're hot, and maybe even active
	if (mouseHit(rectangle)) //See previous part's code for this method
			{
		uistate.hot = id;
		if (uistate.active == -1 && Input::MouseButtonDown[0]) //LeftMouseDown)
			uistate.active = id;
	}
	//Draw the scrollbar
	//Texture2D* scrollbarTexture = GameArt::getInstance().getTextureById(scrollbarTextureId);
	//Texture2D* gripTexture = GameArt::getInstance().getTextureById(gripTextureId);
	GameServices::getInstance().getSpriteBatch()->draw(scrollbarTexture, rectangle, ColorPreset::White); //mDrawColor);

	SpriteBatch* sb = GameServices::getInstance().getSpriteBatch();

	//Position the grip relative on the scrollbar and make sure the grip stays inside the scrollbar
	//Note that the grip's width if vertical/height if horizontal is the scrollbar's smallest dimension.
	int gripPosition;
	Rectangle grip;
	if (horizontal) {
		gripPosition = rectangle.X
				+ (int) ((((rectangle.W - rectangle.H) * (value / max))));
		grip = (Rectangle ) { gripPosition, rectangle.Y, rectangle.H,
						rectangle.H };
	} else {
		gripPosition = Rectangle::getBottom(rectangle) - rectangle.W
				- (int) ((((rectangle.H - rectangle.W) * (value / max))));
		grip = (Rectangle ) { rectangle.X - 1, gripPosition, rectangle.W,
						rectangle.W };
	}
	//Draw the grip in the correct color
	if (uistate.active == id || uistate.hot == id) {
		sb->draw(gripTexture, grip, ColorPreset::White);    //mActiveColor);
	} else {
		sb->draw(gripTexture, grip, ColorPreset::White);    //mDrawColor);
	}
	//If we're active, calculate the new value and do some bookkeeping to make sure the mouse and grip are in sync
	if (uistate.active == id) {
		if (horizontal) {
			//Because the grip's position is defined in the top left corner
			//we need to shrink the movable domain slightly so our grip doesnt
			//draw outside the scrollbar, while still getting a full range.
			float mouseRelative = Input::MousePosition[0].X
					- (rectangle.X + grip.W / 2);
			mouseRelative = std::min(mouseRelative,
					(float) (((rectangle.W - grip.W))));
			mouseRelative = std::max(0.f, mouseRelative);
			//We then calculate the relative mouse offset 0 if the mouse is at
			//the left end or more to the left. 1 if the mouse is at the right end
			//or more to the right. We then multiply this by max to get our new value.
			value = (mouseRelative / (rectangle.W - grip.W)) * max;
		} else {
			//same as horizontal but in the end we inverse value,
			//because we want the bottom to be 0 instead of the top
			//while in y coordinates the top is 0.
			float mouseRelative = Input::MousePosition[0].X
					- (rectangle.Y + grip.H / 2);
			mouseRelative = std::min(mouseRelative,
					(float) (((rectangle.H - grip.H))));
			mouseRelative = std::max(0.f, mouseRelative);
			value = max - (mouseRelative / (rectangle.H - grip.H)) * max;
		}
	}
	return value;
}

void ImGui::canvas(int id, const Rectangle& rect, hxTexture2D* texid, bool flip, hxRectangle*) {
//    int fx = flip ? SpriteEffects::FlipBoth : 0;
//

	GameServices::getInstance().getSpriteBatch()->draw(
			texid, rect,
			NULL, ColorPreset::White, 0, Vector2::Zero, 0, 0.f);
}

bool ImGui::textInput(int id, const Rectangle& rect, hxTexture2D* bgTexture, char* buffer, hxSpriteFont* font, unsigned char alpha, const hxRectangle* src) {
	Color dc = ColorPreset::LightGray;
	if (mouseHit(rect)) {
		uistate.hot = id;
		dc = ColorPreset::White;
		if (uistate.active == 0 && Input::MouseButtonDown[0]) {
			uistate.active = id;
		}
	} else {
		uistate.keychar = 0;
	}

	if (uistate.kbditem == 0)
		uistate.kbditem = id;

	int len = strlen(buffer);

	if (uistate.kbditem == id) {

//		if (uistate.keychar != 0) {
//			int k = 0;
//		}

		if (uistate.keychar == KeyEventArgs::Key_SPACE) {
			//uistate.keychar = 0;
			buffer[len++] = 0x20;

			buffer[len] = '\0';
		}
		if (uistate.keychar == KeyEventArgs::Key_BACKSPACE) {
			if (len > 0) {
				len--;
				buffer[len] = '\0';
				uistate.keychar = 0;
			}
		}

		if (uistate.keychar >= 32 && uistate.keychar < 127) {

			buffer[len++] = (char) uistate.keychar;
			buffer[len] = 0;
			uistate.keychar = 0;
		}
		Input::KeyPressed = 0;

	}

	//SpriteFont* font = GameArt::getInstance().getFontById(fontid);
	float hy = 0.f;
	if (len > 0) {
		Vector2 h = font->measureString(buffer);
		hy = h.Y;
	}

	//Texture2D* bgTexture = GameArt::getInstance().getTextureById(bgTextureId);

	dc.A = alpha;

	SpriteBatch* sb = GameServices::getInstance().getSpriteBatch();

	sb->draw(bgTexture, rect, (hxRectangle*)src, dc, 0, Vector2::Zero, 0, 0.f);
	label(id, (Vector2 ) { 10.f + rect.X, rect.Y + hy * 0.5f + rect.H / 2 }, buffer, font, alpha, false);

    return uistate.hot == id;
}

