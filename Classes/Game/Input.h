#ifndef CLASSES_GAME_INPUT_H_
#define CLASSES_GAME_INPUT_H_

#include <helix/core/Vector2.h>
#include <helix/base/IApplication.h>
#include <helix/core/EventHandler.h>

struct Input {
    static hxVector2 MousePosition[5];
    static hxVector2 MousePrevPosition[5];
    static hxVector2 MousePressPosition[5];
    static hxVector2 MouseReleasePosition[5];
    static bool      MouseButtonDown[5];
    static int       KeyPressed;
    static int 		 DoubleTapTimeout;
    static const int MaxDoubleTapTimeout = 500;
    static int 		 TapCount;

    static void onMousePressed(const hxMouseEventArgs&);
    static void onMouseReleased(const hxMouseEventArgs&);
    static void onMouseMoved(const hxMouseEventArgs&);
    static void onKeyPressed(const hxKeyEventArgs&);
    static void onKeyReleased(const hxKeyEventArgs&);

    static hxEventHandler<hxEventArgs>::Type*	  OnDoubleTap;
    static hxEventHandler<hxMouseEventArgs>::Type OnDragBegin;
    static hxEventHandler<hxMouseEventArgs>::Type OnDragEnd;
};
#endif
