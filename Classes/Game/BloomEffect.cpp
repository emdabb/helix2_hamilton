/**
 * @file BloomEffect.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Oct 20, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include "BloomEffect.h"
#include <helix/content/AssetManager.h>
#include <helix/gfx/SpriteBatch.h>
#include <helix/gfx/DepthStencilBuffer.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/RenderState.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/engine/GameArt.h>
#include <helix/engine/GameServices.h>

using namespace helix::core;
using namespace helix::gfx;
using namespace helix::engine;
using namespace helix::content;

BloomSettings::BloomSettings(const std::string& name, float bloomThresh, float blurAmount, float bloomIntensity, float baseIntensity, float bloomSaturation, float baseSaturation)
: Name(name)
, BloomThreshold(bloomThresh)
, BlurAmount(blurAmount)
, BloomIntensity(bloomIntensity)
, BaseIntensity(baseIntensity)
, BloomSaturation(bloomSaturation)
, BaseSaturation(baseSaturation)
{
}


BloomSettings* BloomSettings::Presets[] = {
        //                Name           Thresh  Blur Bloom  Base  BloomSat BaseSat
        new BloomSettings("Default",     0.25f,  3,   1.25f, 1,    1,       1),
        new BloomSettings("Soft",        0,      3,   1,     1,    1,       1.f),
        new BloomSettings("Desaturated", 0.5f,   8,   1,     1,    0,       1.f),
        new BloomSettings("Saturated",   0.25f,  4,   2,     1,    2,       0.f),
        new BloomSettings("Blurry",      0,      2,   1,     0.1f, 1,       1.f),
        new BloomSettings("Subtle",      0.5f,   2,   1,     1,    1,       1.f),
};



BloomEffect::BloomEffect(IGraphicsDevice* dev, unsigned int w, unsigned int h)
: mDevice(dev)
, mWidth(w)
, mHeight(h)
, mRenderTarget1(NULL)
, mRenderTarget2(NULL)
{
    mTransform = Matrix::Identity;
    mSettings = BloomSettings::Presets[0];
    mSpriteBatch = new SpriteBatch(dev); //GameServices::spriteBatch;//
    //mQuad = new FullscreenQuad(dev);

    int width 	= mWidth >> 1;
	int height 	= mHeight >> 1;
	mRenderTarget1 = new Texture2D(mDevice, width, height, PixelFormat::Color, false);
	mRenderTarget2 = new Texture2D(mDevice, width, height, PixelFormat::Color, false);

	setBlurEffectParameters(0, 1.f / (float)width, 	0					);
	setBlurEffectParameters(1, 0, 					1.f / (float)height	);


    mBloomExtractShader = GameArt::getInstance().getShaderProgram("bloomExtract");
    mBloomCombineShader = GameArt::getInstance().getShaderProgram("bloomCombine");
    mBlurShader = GameArt::getInstance().getShaderProgram("gaussianBlur");


    weightsParameter = mBlurShader->getUniformByName<float>("_SampleWeight[0]");
    offsetsParameter = mBlurShader->getUniformByName<Vector2>("_SampleOffset[0]");

}

void BloomEffect::draw(Texture2D* sceneTexture, Texture2D* dst) {
    DepthStencilBuffer* oldBuffer = NULL;
#if 0
    mDevice->getDepthStencilBuffer(&oldBuffer);//DepthStencilBuffer(&oldBuffer);
    mDevice->setDepthStencilBuffer(NULL);//setDepthStencilBuffer(NULL);
#endif
    /**
     *
     * Pass 1: draw the scene into rendertarget 1, using a shader that extracts only the brightest parts of the image.
     *
     */
    mBloomExtractShader->getUniformByName<float>("_BloomThreshold")->setValue(const_cast<float*>(&mSettings->BloomThreshold));
    drawFullscreenQuad(sceneTexture, mRenderTarget1, mBloomExtractShader, PreBloom);
    /**
     *
     * Pass 2: draw from rendertarget 1 into rendertarget 2, using a shader to apply a horizontal gaussian blur filter.
     *
     */
    weightsParameter->setValue(mSampleWeights[E_BLUR_H]);
    offsetsParameter->setValue(mSampleOffsets[E_BLUR_H]);
    drawFullscreenQuad(mRenderTarget1, mRenderTarget2, mBlurShader, BlurredHorizontally);
    /**
     *
     *
     * Pass 3: draw from rendertarget 2 back into rendertarget 1, using a shader to apply a vertical gaussian blur filter.
     *
     */
    weightsParameter->setValue(mSampleWeights[E_BLUR_V]);//, SAMPLE_COUNT);
    offsetsParameter->setValue(mSampleOffsets[E_BLUR_V]);//, SAMPLE_COUNT);
    drawFullscreenQuad(mRenderTarget2, mRenderTarget1, mBlurShader, Blurred);

    /**
     *
     * Pass 4: draw both rendertarget 1 and the original scene image back into the main backbuffer, using a shader that
     * combines them to produce the final bloomed result.
     *
     */
    Vector4 vBloomSettings = {
            mSettings->BloomSaturation,
            mSettings->BloomIntensity,
            mSettings->BaseSaturation,
            mSettings->BaseIntensity
    };
    ShaderParameter<Vector4>* mBloomSettingsParameter = NULL;
    mBloomSettingsParameter = mBloomCombineShader->getUniformByName<Vector4>("_BloomSettings");
    mBloomSettingsParameter->setValue(&vBloomSettings);

    Sampler2D sBloomSampler;
    sBloomSampler.TextureRef = mRenderTarget1;
    sBloomSampler.SamplerID = 1;

    mBloomCombineShader->getUniformByName<Sampler2D>("BloomSampler")->setValue(&sBloomSampler);

    if(dst != NULL) {
        this->mDevice->setRenderTarget(dst, 0);
        this->mDevice->clear(ColorPreset::Black, ClearOptions::ClearAll, 1.0f, 0);
        drawFullscreenQuad(sceneTexture, mWidth, mHeight, mBloomCombineShader, FinalResult);
        this->mDevice->resolveRenderTarget(0);
    } else {
    	drawFullscreenQuad(sceneTexture, mWidth, mHeight, mBloomCombineShader, FinalResult);
    }
}

void BloomEffect::drawFullscreenQuad(Texture2D* texture, int width, int height, ShaderProgram* effect, int currentBuffer, const int spriteEffects) {
    // If the user has selected one of the show intermediate buffer options,
    // we still draw the quad to make sure the image will end up on the screen,
    // but might need to skip applying the custom pixel shader.
    if (currentBuffer > FinalResult) {
        effect = NULL;
    }
    Viewport oldViewport = mDevice->getViewport();
    Viewport vp;
    vp.X = 0;
    vp.Y = 0;
    vp.W = width;
    vp.H = height;
    mDevice->setViewport(vp);
    mSpriteBatch->begin(0, BlendState::Opaque, Matrix::Identity, effect);
    mSpriteBatch->draw(texture, vp, NULL, ColorPreset::White, 0.0f, Vector2::Zero, SpriteEffect::FlipBoth, 0.0f);
    mSpriteBatch->end();
    mDevice->setViewport(oldViewport);
}

void BloomEffect::drawFullscreenQuad(Texture2D* texture, Texture2D* renderTarget, ShaderProgram* effect, int currentBuffer) {
    mDevice->setRenderTarget(renderTarget, 0);
    mDevice->clear(ColorPreset::Black, ClearOptions::ClearAll, 0, 1.f);
    drawFullscreenQuad(texture, renderTarget->getWidth(), renderTarget->getHeight(), effect, currentBuffer);
    mDevice->resolveRenderTarget(0);
}

float BloomEffect::computeGaussian(float n) {

    float theta = mSettings->BlurAmount;
    // return (float) (1.0 / (sqrt_2pi * theta)) * (real_exp(-(n * n) / (2 * theta * theta)));
    return (float) ((1.0 / real_sqrt(2 * MathHelper::PI * theta)) * real_exp(-(n * n) / (2 * theta * theta)));
}

void BloomEffect::setBlurEffectParameters(int n, float dx, float dy) {

    // Look up how many samples our gaussian blur effect supports.
    int sampleCount = SAMPLE_COUNT; //weightsParameter.Elements.Count;
    // Create temporary arrays for computing our filter settings.
    float* sampleWeights = mSampleWeights[n];//[sampleCount];
    Vector2* sampleOffsets = mSampleOffsets[n];//[sampleCount];
    // The first sample always has a zero offset.
    sampleWeights[0] = computeGaussian(0);
    sampleOffsets[0] = Vector2::Zero;
    // Maintain a sum of all the weighting values.
    float totalWeights = sampleWeights[0];
    // Add pairs of additional sample taps, positioned
    // along a line in both directions from the center.
    for (int i = 0; i < sampleCount / 2; i++) {
        // Store weights for the positive and negative taps.
        float weight = computeGaussian(i + 1);
        sampleWeights[i * 2 + 1] = weight;
        sampleWeights[i * 2 + 2] = weight;
        totalWeights += weight * 2;
        // To get the maximum amount of blurring from a limited number of
        // pixel shader samples, we take advantage of the bilinear filtering
        // hardware inside the texture fetch unit. If we position our texture
        // coordinates exactly halfway between two texels, the filtering unit
        // will average them for us, giving two samples for the price of one.
        // This allows us to step in units of two texels per sample, rather
        // than just one at a time. The 1.5 offset kicks things off by
        // positioning us nicely in between two texels.
        float sampleOffset = i * 2 + 1.5f;
        Vector2 delta = { dx * sampleOffset, dy * sampleOffset };
        // Store texture coordinate offsets for the positive and negative taps.
        sampleOffsets[i * 2 + 1] = delta;
        sampleOffsets[i * 2 + 2] = -delta;
    }
    // Normalize the list of sample weightings, so they will always sum to one.
    for (int i = 0; i < sampleCount; i++) {
        sampleWeights[i] /= totalWeights;
        //sampleWeights[i] *= 1.2f;
    }
    float sum = 0.f;
    for(int i=0; i < sampleCount; i++) {
        sum+=sampleWeights[i];
    }


    // Tell the effect about our new filter settings.

}
