#include "Input.h"

hxVector2 Input::MousePosition[5] = { hxVector2::Zero };
hxVector2 Input::MousePrevPosition[5] = { hxVector2::Zero };
hxVector2 Input::MousePressPosition[5] = { hxVector2::Zero };
hxVector2 Input::MouseReleasePosition[5] = { hxVector2::Zero };
bool      Input::MouseButtonDown[5] = { false };
int       Input::KeyPressed = -1;
int		  Input::DoubleTapTimeout = 0;
int		  Input::TapCount = 0;
hxEventHandler<hxEventArgs>::Type* Input::OnDoubleTap = new hxEventHandler<hxEventArgs>::Type();

void Input::onMousePressed(const hxMouseEventArgs& args) {
	Input::MouseButtonDown[args.PointerId] = true;
	Input::MousePressPosition[args.PointerId].X = args.X;
	Input::MousePressPosition[args.PointerId].Y = args.Y;
//    Input::MousePosition[args.PointerId].X = args.X;
//    Input::MousePosition[args.PointerId].Y = args.Y;

}
void Input::onMouseReleased(const hxMouseEventArgs& args){

	if(Input::MouseButtonDown[args.PointerId] != false) {
		DoubleTapTimeout = MaxDoubleTapTimeout;
		TapCount++;
		if(TapCount == 2) {
			OnDoubleTap->operator ()(hxEventArgs::Empty);
			TapCount = 0;
		}
	}

    Input::MouseButtonDown[args.PointerId] = false;
    Input::MouseReleasePosition[args.PointerId].X = args.X;
    Input::MouseReleasePosition[args.PointerId].Y = args.Y;
}
void Input::onMouseMoved(const hxMouseEventArgs& args) {

	Input::MousePrevPosition[args.PointerId].X = Input::MousePosition[args.PointerId].X;
	Input::MousePrevPosition[args.PointerId].Y = Input::MousePosition[args.PointerId].Y;

    Input::MousePosition[args.PointerId].X = args.X;
    Input::MousePosition[args.PointerId].Y = args.Y;
}
void Input::onKeyPressed(const hxKeyEventArgs& args) {
    Input::KeyPressed = args.KeyId;
}
void Input::onKeyReleased(const hxKeyEventArgs& args) {
    Input::KeyPressed = -1;
}
