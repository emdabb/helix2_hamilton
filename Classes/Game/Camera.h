/*
 * Camera.h
 *
 *  Created on: Aug 14, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_CAMERA_H_
#define CLASSES_GAME_CAMERA_H_

#include <helix/core/Matrix.h>
#include <helix/core/Vector3.h>
#include <helix/core/Quaternion.h>
#include <cstring>

using namespace helix::core;

class Camera {
	Matrix mTransform;
	Matrix mProjection;

	Vector3 mPosition;
	Vector3 mLookAt;

	Vector3 mRotation;

	Quaternion mOrientation;

	bool mIsTransformChanged;
	bool mIsProjectionChanged;
protected:
	void updateTransform() {
        
        Quaternion q;
        Quaternion::createfromYawPitchRoll(mRotation.X, mRotation.Y, 0.0f, &q);
        Vector3 up = Vector3::transform(Vector3::Up, q);
        Vector3 pos = Vector3::transform(Vector3::Forward * 5, q);

		Matrix::createLookat(pos, mLookAt, up, &mTransform);

	}
	void updateProjection() {
		Matrix::createPerspectiveFov(45.0f, 1.33f, 0.1f, 100.0f, &mProjection);
	}
public:

	Camera()
	: mIsTransformChanged(true)
	, mIsProjectionChanged(true)
	{
		setPosition(Vector3::Forward * -5.f);
		lookAt(Vector3::Zero);
	}

	virtual ~Camera() {

	}

	void getProjection(Matrix* mat) {
		if(mIsProjectionChanged) {
			updateProjection();
			mIsProjectionChanged = false;
		}
		memcpy(mat, &mProjection, sizeof(Matrix));
	}

	void getTransform(Matrix* mat) {
		if(mIsTransformChanged) {
			updateTransform();
			mIsTransformChanged = false;
		}
		memcpy(mat, &mTransform, sizeof(Matrix));
	}

	void lookAt(const Vector3& vec) {
		mLookAt = vec;
		mIsTransformChanged = true;
	}

	void setPosition(const Vector3& vec) {
		mPosition = vec;
		mIsTransformChanged = true;
	}

	void setRotation(const Vector3& vec) {
		mRotation = vec;
		mIsTransformChanged = true;
	}

};



#endif /* CLASSES_GAME_CAMERA_H_ */
