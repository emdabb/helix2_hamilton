#ifndef AGARIO_UTILITY_H_
#define AGARIO_UTILITY_H_

#include <helix/Types.h>
#include <iostream>
#include <helix/gfx/SpriteBatch.h>
#include <helix/core/Vector2.h>

struct World;

HXAPI HXEXPORT int timer();
HXAPI HXEXPORT int colorHsv(unsigned char, unsigned char, unsigned char);
HXAPI HXEXPORT void loadObj(World*, std::istream*);
HXAPI HXEXPORT void SpriteBatch_drawLine(hxSpriteBatch* spriteBatch, const hxVector2& start, const hxVector2& end, const hxColor& tint, float thickness);


#endif
