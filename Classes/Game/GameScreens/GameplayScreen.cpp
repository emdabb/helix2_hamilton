/*
 * GameplayScreen.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <helix/engine/GameScreen.h>
#include <helix/engine/GameServices.h>
#include <helix/engine/gui/GuiContainer.h>
#include <helix/engine/StateMachine.h>
#include <helix/engine/GameArt.h>
#include <Classes/Game/Input.h>
#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Utility.h>
#include <Classes/Game/Camera.h>
#include <Classes/Game/ImGui.h>
#include <string.h>

using namespace helix::core;
using namespace helix::engine;

static const char* levelNames[] = {
   	"tetrahedron",
    "hexahedron",
    "octahedron",
	"icosahedron",
	"truncated_cube"

};

const char* levelStrings[] = {
	"(I) The world of being; \neverything in this world always is, \nhas no becoming, \nand does not change",
	"(II) It is apprehended by the understanding, not by the senses.",
	"(III) The world of becoming; \neverything in this world comes to be and passes away, \nbut never really is",
	"(IV) The cosmos itself came into being, \ncreated using as its model the world of Forms.",
	"(V)",
	"(VI)",
	"(VII)",
	"(VIII)",
	"(IX)"
};

HXAPI GameScreen* new_MessageBox(const char* str);

struct GameplayState_levelBegin : public IState<GameScreen> {
    void onEnter(GameScreen* ) {

    }
    void onUpdate(GameScreen* screen, int dt) {

    }

    void onExit(GameScreen* screen) {

    }
};

typedef enum {
    FadeIn,
    FadeInComplete,
    Gameplay,
    FadeOut,
    FadeOutComplete,
    PendingMessageBox
} GameplayState;

class GameplayScreen : public GameScreen {
	GuiContainer* mContainer;
	World mWorld;
	Camera mCamera;
	Vector3 mRotation;
	Vector3 mOldRotation;
    int mState;
    int mStateTime;
    float mStateAlpha;
    const static int LevelBeginTime = 1500;
    const static int LevelEndTime = LevelBeginTime;
protected:
	void onLevelCompleted(const int& args) {
        mStateTime = 0;
        mState = FadeOut;
	}
public:
	GameplayScreen()
	: mContainer(NULL)
    , mStateTime(0)
    , mStateAlpha(1.0f)
	{
		mWorld.OnLevelCompleted += event(this, &GameplayScreen::onLevelCompleted);

	}

	virtual ~GameplayScreen() {

	}

	virtual void loadContent() {
		World_initialize(&mWorld);
	}

    void handleInput() {
        static bool isDragging = false;

        static Vector2 newRotation = (Vector2) { 0, 0 };

        mOldRotation = mRotation;

        if(Input::MouseButtonDown[0] != false) {
            isDragging = true;
        } else {
            isDragging = false;
        }
        if(isDragging) {

            float fx = (Input::MousePrevPosition[0].X - Input::MousePosition[0].X);
            float fy = (Input::MousePrevPosition[0].Y - Input::MousePosition[0].Y);
            if(real_abs(fx) > 0.001f || real_abs(fy) > 0.001f) {
                newRotation.X = (Input::MousePrevPosition[0].X - Input::MousePosition[0].X) / 1024.f;
                newRotation.Y = (Input::MousePrevPosition[0].Y - Input::MousePosition[0].Y) / 768.f;
            }

			mRotation.X += newRotation.X;//MathHelper::lerp(mOldRotation.X, newRotation.X, 0.025f);
			mRotation.Y += newRotation.Y;//MathHelper::lerp(mOldRotation.Y, newRotation.Y, 0.025f);
			mRotation.Z = 0.0f;

			mCamera.setRotation(mRotation);

        }

    }

    void onMessageBoxExit(const hxEventArgs& args) {
        char path[256] = { 0 };
        strcat(path, "models/");
        strcat(path, levelNames[mWorld.currentLevel]);
        strcat(path, ".mdl");
        World_destroy(&mWorld);
        loadObj(&mWorld, GameServices::getInstance().getAssetManager()->openStream(path));
        World_validateMove(&mWorld, 0);
        mState = FadeIn;
    }

	virtual void update(int dt, bool a, bool b) {
		GameScreen::update(dt, a, b);
        switch(mState) {
            case FadeIn:
                if(mStateTime <= 0) {
                    mStateTime = 0;
                    mStateTime = 0;
                    mState = FadeInComplete;
                    break;
                }
                mStateAlpha = ((float)mStateTime / (float)LevelBeginTime);
                mStateTime -= dt;
                break;
            case FadeInComplete:
                mState = Gameplay;
                break;
            case Gameplay:
                World_update(&mWorld, dt, &mCamera);
                mStateAlpha = 1.0f - getTransitionAlpha();
                mStateTime  = 0;
                break;
            case FadeOut:
                if(mStateTime >= LevelEndTime) {
                    mStateTime = LevelEndTime;
                    mState = FadeOutComplete;
                    break;
                }
                mStateAlpha = ((float)mStateTime / (float)LevelBeginTime);
                mStateTime += dt;
                break;
            case FadeOutComplete:

                mState = PendingMessageBox;
                GameScreen* gs = new_MessageBox(levelStrings[mWorld.currentLevel]);
                gs->OnExit += event(this, &GameplayScreen::onMessageBoxExit);
                mOwner->addScreen(gs);

                break;

        }




	}
	virtual void draw(int dt) {

        hxColor color = hxColorPreset::Black;
        color.A = (char)(mStateAlpha * 255);
        if(mState != PendingMessageBox) {
            World_draw(&mWorld, &mCamera);

            char buf[128] = { 0 };
            //strcat(buf, levelNames[mWorld.currentLevel]);
            sprintf(buf, "the %s [%d]", levelNames[mWorld.currentLevel], mWorld.numMoves - 1);


            ImGui::beginGui(Matrix::Identity);
            ImGui::label(UID, (Vector2) { 0, 64 }, buf, GameArt::getInstance().getFontById(0), 255, false, NULL);
            ImGui::endGui();

        }
		GameScreen::draw(dt);
        mOwner->drawRectangle((Rectangle) { 0, 0, 1024, 768 }, color);

	}
};

HXAPI GameScreen* new_GameplayScreen() {
	return new GameplayScreen;
}





