/*
 * MessageBox.cpp
 *
 *  Created on: Jul 21, 2015
 *      Author: miel
 */

#include <helix/Types.h>
#include <helix/engine/GameScreen.h>
#include <helix/engine/gui/GuiContainer.h>
#include <helix/engine/gui/GuiLabel.h>
#include <helix/engine/gui/GuiGridLayout.h>
#include <helix/engine/gui/GuiBorderLayout.h>
#include <helix/engine/GameArt.h>
#include <helix/engine/GameServices.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/Viewport.h>
#include <helix/core/Easing.h>
#include <Classes/Game/Input.h>
#include <Classes/Game/ImGui.h>
#include <string>

using namespace helix::engine;
using namespace helix::gfx;
using namespace helix::core;

class MessageBox : public GameScreen {
//	GuiContainer* 	mContainer;
//	GuiLabel* 		mLabel;
//	GuiButton*		mButton;
	const char* mText;
public:
	MessageBox(const char* text)
	//: mContainer(new GuiContainer)
	: mText(text)
	{
//		hxSpriteFont* font = GameArt::getInstance().getFont("tinyFont");
//		hxTexture2D* tex = GameArt::getInstance().getTexture2D("whitePixel");
//		mLabel->setFont(font);
//		mButton->setFont(font);
//
//		mContainer->setBackgroundImage(tex, NULL);
//		mLabel->setBackgroundImage(tex, NULL);
//		mButton->setBackgroundImage(tex, NULL);
//
//		mContainer->setBackgroundColor(ColorPreset::Black);
//		mButton->setBackgroundColor(ColorPreset::Black);
//		mLabel->setBackgroundColor(ColorPreset::Black);
//
//		mLabel->setForegroundColor(ColorPreset::LimeGreen);
//        mButton->setForegroundColor(ColorPreset::LimeGreen);
//
//        mContainer->setLayoutManager(new GuiBorderLayout);
//		mContainer->add(mLabel, GuiBorderLayout::Layout_Center);
//		//mContainer->add(mButton, GuiBorderLayout::Layout_South);
//
//		mLabel->OnClick += event(this, &MessageBox::onClick);

	}

	virtual ~MessageBox() {

	}

	void loadContent() {

	}

	void unloadContent() {

	}

	void onClick(const GuiComponentEventArgs& args) {
		exitScreen();
	}

	void update(int dt, bool a, bool b) {
		static int t = 0;
		IGraphicsDevice* device = GameServices::getInstance().getGraphicsDevice();
		Viewport vp = device->getViewport();

		if(Input::MouseButtonDown[0]) {
			exitScreen();
		}

		int sx = vp.W >> 1;
		int sy = vp.H >> 2;



//		mContainer->setAlpha(getTransitionAlpha());
//		mContainer->setSize(sx, (int)((float)sy * fItp));
//		mContainer->setLocation((vp.W >> 1) - (sx >> 1), (vp.H >> 1) - (sy >> 1));
//		mContainer->update(dt);

		GameScreen::update(dt, a, b);

		t+= dt;

	}

	void draw(int) {
		IGraphicsDevice* pDevice = GameServices::getInstance().getGraphicsDevice();
		Viewport vp = pDevice->getViewport();

		char aa = (char)(getTransitionAlpha()* 255);
		hxSpriteFont* font = GameArt::getInstance().getFont("tinyFont");
		ImGui::beginGui(Matrix::Identity);
		ImGui::label(UID, (Vector2) { (float)(vp.W / 2), (float)(vp.H / 2) }, mText, font, aa, true, NULL);
		ImGui::endGui();
	}
};

HXAPI HXEXPORT GameScreen* new_MessageBox(const char* str) {
	return new MessageBox(str);
}
