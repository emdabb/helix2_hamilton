/**
 * @file BloomEffect.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Oct 20, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef BLOOMEFFECT_H_
#define BLOOMEFFECT_H_

#include <string>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/Sampler2D.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/ShaderParameter.h>
#include <helix/gfx/SpriteBatch.h>
#include <helix/gfx/ClearOptions.h>
#include <helix/core/Vector2.h>
#include <helix/core/Matrix.h>
#include <helix/content/AssetManager.h>


struct BloomSettings {
    const std::string Name;
    const float BloomThreshold;
    const float BlurAmount;
    union {
        struct {
            const float BloomIntensity;
            const float BaseIntensity;
            const float BloomSaturation;
            const float BaseSaturation;
        };
        const float Bloom[4];
    };

    BloomSettings(const std::string& name, float bloomThresh, float blurAmount, float bloomIntensity, float baseIntensity, float bloomSaturation, float baseSaturation);

    static BloomSettings* Presets[];
};

class BloomEffect {
    enum {
        E_BLUR_H,
        E_BLUR_V
    };
    //FullscreenQuad* mQuad;
    static const int SAMPLE_COUNT = 9;
    hxTexture2D* 					 	 mRenderTarget1;
    hxTexture2D* 					 	 mRenderTarget2;
    BloomSettings* 						 mSettings;
    hxGraphicsDevice* 					 mDevice;
    hxSpriteBatch* 				 		 mSpriteBatch;
    hxShaderProgram*                     mBloomExtractShader;
    hxShaderProgram*                     mBlurShader;
    hxShaderProgram*                     mBloomCombineShader;
    hxShaderParameter<float>::Type* 	 weightsParameter;
    hxShaderParameter<hxVector2>::Type*  offsetsParameter;
    hxVector2						 	 mSampleOffsets[2][SAMPLE_COUNT];
    float								 mSampleWeights[2][SAMPLE_COUNT];
    unsigned int						 mWidth;
    unsigned int 						 mHeight;
    hxMatrix							 mTransform;
public:
    enum {
        PreBloom,
		BlurredHorizontally,
		Blurred,
		FinalResult
    };
public:
    BloomEffect(hxGraphicsDevice* dev, unsigned int width, unsigned int height);


    void draw(hxTexture2D* sceneTexture, hxTexture2D* dst = NULL);
    void drawFullscreenQuad(hxTexture2D* texture, hxTexture2D* renderTarget, hxShaderProgram* effect, int currentBuffer);
    void drawFullscreenQuad(hxTexture2D* texture, int width, int height, hxShaderProgram* effect, int currentBuffer, const int fx = 0);
    float computeGaussian(float n);
    void setBlurEffectParameters(int n, float dx, float dy);
};



#endif /* BLOOMEFFECT_H_ */
