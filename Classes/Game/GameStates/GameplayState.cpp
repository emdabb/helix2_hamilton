/*
 * GameplayState.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <helix/engine/StateMachine.h>
#include <Classes/Game/Game.h>

using namespace helix::engine;

HXAPI GameScreen* new_GameplayScreen();

class GameplayState : public IState<Game> {
	GameScreen* mScreen;
public:
	virtual ~GameplayState() {

	}

	virtual void onEnter(Game* game) {
		GameScreenManager& screens = game->getScreenManager();
		mScreen = new_GameplayScreen();
		screens.addScreen(mScreen);
	}

	virtual void onUpdate(Game*, int) {

	}

	virtual void onExit(Game*) {
		mScreen->exitScreen();
	}

};


