/*
 * LoadingState.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <helix/engine/StateMachine.h>
#include <Classes/Game/Game.h>

using namespace helix::engine;

class SplashState : public IState<Game> {

public:
	virtual ~SplashState() {

	}

	virtual void onEnter(Game*) {

	}

	virtual void onUpdate(Game*, int) {

	}

	virtual void onExit(Game*) {

	}

};



