#include <helix/Types.h>
#include <sys/time.h>
#include <unistd.h>
#include <helix/gfx/Color.h>
#include <helix/engine/GameArt.h>
#include <helix/core/DebugLog.h>
#include <helix/core/RandomNumber.h>
#include "Utility.h"
#include <Classes/Game/Ecs/World.h>
#include <sstream>

using namespace helix::gfx;
using namespace helix::core;
using namespace helix::engine;

HXAPI eid new_Edge(World* world, int a, int b, float len);
HXAPI eid new_Vertex(World* world, float x, float y, float z, const Color& color);

static Color randomColorHSV() {
	static RandomNumber rnd;
	char h = (char)(rnd.next() * 255);
	char s = 255;
	char v = 255;
	Color out;
	out.Value = colorHsv(h, s, v);
    return out;
}

HXAPI int timer() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return (int)((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}

HXAPI int makeColor(unsigned char r, unsigned char g, unsigned char b) {
    Color c;
    c.R = r;
    c.G = g;
    c.B = b;
    c.A = 255;
    int res = c.Value;
    return res;
}

HXAPI int colorHsv(unsigned char h, unsigned char s, unsigned char v) {
    unsigned char r, g, b;
    if(s == 0) {
        r = g = b = v;
        return makeColor(r, g, b);
    }
    unsigned char region = h / 43;
    unsigned char fpart  = (h - (region * 43)) * 6;
    unsigned char p = (v * (255 - s)) >> 8;
    unsigned char q = (v * (255 - ((s * fpart) >> 8))) >> 8;
    unsigned char t = (v * (255 - ((s * (255 - fpart)) >> 8))) >> 8;
    switch(region) {
        case 0:
        return makeColor(v, t, p);
        case 1:
        return makeColor(q, v, p);
        case 2:
        return makeColor(p, v, t);
        case 3:
        return makeColor(p, q, v);
        case 4:
        return makeColor(t, p, v);
        default:
        return makeColor(v, p, q);
    }
    return makeColor(0, 0, 0);
}

struct Vertex {
    Vector3 position;
    Vector3 normal;
    Vector2 texCoord;
};

struct VertexRef {
    int v, vn, vt;
};

struct FaceData {
    std::vector<VertexRef> mRef;
};

HXAPI void loadObj(World* world, std::istream* in) {
	DEBUG_METHOD();
#if 1
	std::vector<Vertex> verts;
	std::vector<Vector3> positions;
	std::vector<Vector3> normals;
	std::vector<Vector2> texCoords;
	//std::vector<VertexRef> refs;
	std::vector<FaceData> faces;

	std::string lineStr;
	while(std::getline(*in, lineStr)) {
		std::istringstream lineSS(lineStr);
		std::string lineType;
		lineSS >> lineType;

		if(lineType == "v") {
			float x, y, z, w;
			lineSS >> x >> y >> z >> w;
			Vector3 vPos = { x, y, z };
			positions.push_back(vPos);
		}
		else if(lineType == "vn") {
			float x, y, z;
			lineSS >> x >> y >> z;
			Vector3 vNorm = { x, y, z };
			normals.push_back(vNorm);
		}
		else if(lineType == "vt") {
			float u, v, w;
			lineSS >> u >> v >> w;
			Vector2 vTexCoord = { u, v };
			texCoords.push_back(vTexCoord);
		}
		else if(lineType == "f") {

			FaceData fd;
			int i = 0;

			std::string refStr;
			while(lineSS >> refStr) {
				std::istringstream ref(refStr);
				std::string vStr, vtStr, vnStr;
				std::getline(ref, vStr, '/');
				std::getline(ref, vtStr, '/');
				std::getline(ref, vnStr, '/');

				int currentRef = i;

				VertexRef vRef;
				vRef.v  = atoi(vStr.c_str())  - 1; // -1 because OBJ is 1-indexed
				vRef.vt = atoi(vtStr.c_str()) - 1;
				vRef.vn = atoi(vnStr.c_str()) - 1;
				fd.mRef.push_back(vRef);
			}

			faces.push_back(fd);
		}
	}

	std::vector<eid> vertexID;

	for(size_t i=0; i < positions.size(); i++) {
		eid vid = new_Vertex(world, positions[i].X, positions[i].Y, positions[i].Z, randomColorHSV());
		vertexID.push_back(vid);
	}
	size_t numEdges = 0;
	for(unsigned int i=0; i < faces.size(); i++) {

		for(unsigned int j=0; j < faces[i].mRef.size() - 1; j++) {
			int indexA = faces[i].mRef[j + 0].v;
			int indexB = faces[i].mRef[j + 1].v;

			eid vA = vertexID[indexA];
			eid vB = vertexID[indexB];

			if(!World_isSharedEdgeBetween(world, vA, vB)) {
				float len = World_getDistanceBetween(world, vA, vB);
				new_Edge(world, vA, vB, len + 0.25f);
				numEdges++;
			}
		}
	}
	DEBUG_VALUE_AND_TYPE_OF(vertexID.size());
	DEBUG_VALUE_AND_TYPE_OF(numEdges);
#else

		/**
		 * Read the file's contents into memory.
		 */
		in.seekg(0, std::ifstream::beg);
		int beg = in.tellg();
		in.seekg(0, std::ifstream::end);
		int len = in.tellg() - beg;
		in.seekg(0, std::ifstream::beg);

		char* buf = new char[len];
		in.read(buf, len);
		//in.close();
		/**
		 * Continue.
		 */
		std::vector<face_data> faces;
		std::vector<entity_id> vertices;
		int numIndices = 0;

		char * line = strtok(strdup(buf), "\n");
		while(line) {


		   if(line[0] == 'v') {
			   if(line[1] == 't') {
				   /**
					* Line contains texture coordinate.
					*/
			   }
			   else if(line[1] == 'n') {
				   /**
					* Line contains normal.
					*/
			   }
			   else {
				   /**
					* Line contains vertex data.
					*/


				   float x, y, z;
				   sscanf(line, "v %f %f %f", &x, &y, &z);
				   entity_id vid = new_Vertex(world, x, y, z, randomColorHSV());

				   vertices.push_back(vid);


			   }
		   }

		   if(line[0] == 'f') {
			   /**
				* Face data encountered.
				*/
			   face_data fd;

			   int n = -1;

			   std::stringstream ss;
			   ss.str(&line[0]);

			   do {
				   for(int number; ss >> number;) {
					   // -1 because OBJ files are 1 indexed.
					   fd.indices.push_back(number - 1);
				   }
				   if(ss.fail()) {
					   ss.clear();
					   std::string token;
					   ss >> token;
				   }
			   } while(!ss.eof());

			   faces.push_back(fd);
		   }

		   line  = strtok(NULL, "\n");
		}

		int numEdges = 0;
		size_t numFaces = faces.size();
		for(size_t i=0; i < numFaces; i++) {
			size_t numIndicesForFace = faces[i].indices.size();

			for(int j=0; j < (int)numIndicesForFace - 1; j++) {

				int indexA = faces[i].indices[j];
				int indexB = faces[i].indices[j + 1];

				entity_id vA = vertices[indexA];
				entity_id vB = vertices[indexB];

				if(!World_isSharedEdgeBetween(world, vA, vB)) {
					float len = 0;
					World_getDistance(world, vA, vB, &len);
					new_Edge(world, vA, vB, len + 0.25f);
					numEdges++;
				}
			}
	   }

		delete[] buf;
#endif
}

HXAPI void SpriteBatch_drawLine(hxSpriteBatch* spriteBatch, const hxVector2& start, const hxVector2& end, const hxColor& tint, float thickness) {
    Vector2 delta = end - start;
    spriteBatch->draw(
    		GameArt::getInstance().getTexture2D("whitePixel"),
			start,
			NULL, tint,
			Vector2::toAngle(delta),
			(Vector2){0, 0.5f * thickness},
			(Vector2){Vector2::length(delta), thickness},
			0,
			0.f);
}
