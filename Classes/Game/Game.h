/*
 * Game.h
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_GAME_H_
#define CLASSES_GAME_GAME_H_

#include <helix/base/IApplet.h>
#include <helix/engine/StateMachine.h>
#include <helix/engine/GameScreen.h>

using namespace helix::engine;

struct Game : public hxApplet {
	virtual ~Game() {}

	virtual GameScreenManager& getScreenManager() = 0;
	virtual StateMachine<Game>& getStateMachine() = 0;
};



#endif /* CLASSES_GAME_GAME_H_ */
