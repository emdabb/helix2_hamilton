/*
 * World.h
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_ECS_WORLD_H_
#define CLASSES_GAME_ECS_WORLD_H_

#include <helix/core/Vector3.h>
#include <helix/core/Vector2.h>
#include <helix/core/EventHandler.h>
#include <helix/gfx/Color.h>
#include <Classes/Game/Ecs/Entity.h>

#include <vector>

#define _MAX_DEGREE		5

typedef eid edge_cache[_MAX_DEGREE];

class Camera;

struct constraint_t {
	int a, b;
	float restLen;
};

struct visual_t {
	int 		texRef;
	hxColor 	color;
	hxVector2	pivot;
};

struct transform_t {
	hxVector3 	x;
	hxVector3 	oldx;
	float 		scale;
};

struct physics_t {
	hxVector3 	v;
	hxVector3 	a;
};

struct tween_t {
	int 	tweenDelegateId;
	int 	currentTime;
	int 	maxTime;
	int 	direction;
	float* 	valuePtr;
	float 	startValue;
	float 	endValue;
};

struct World;

struct TouchEventArgs {
	World* 	world;
	eid		id;
};

struct touchable_t {
	float Radius;
	hxEventHandler<TouchEventArgs>::Type OnTouchBegin;
	hxEventHandler<TouchEventArgs>::Type OnTouchEnd;
};

typedef struct World {
	bitmask_t	mask[_NUM_ENTITIES];
	constraint_t constraint[_NUM_ENTITIES];
	transform_t	transform[_NUM_ENTITIES];
	physics_t	physics[_NUM_ENTITIES];
	visual_t	visual[_NUM_ENTITIES];
	touchable_t touchable[_NUM_ENTITIES];
	tween_t		tween[_NUM_ENTITIES];
	bool		isExpired[_NUM_ENTITIES];
	bool		isVisited[_NUM_ENTITIES];
	edge_cache	edges[_NUM_ENTITIES];
	std::vector<eid> visited;
	int			numMoves;
	int			currentLevel;

	hxEventHandler<int>::Type OnLevelCompleted;
} *world_t;

HXAPI HXEXPORT void  World_initialize(World*);
HXAPI HXEXPORT void  World_destroy(World*);
HXAPI HXEXPORT eid   World_createEntity(World*);
HXAPI HXEXPORT void  World_destroyEntity(World*, const eid&);
HXAPI HXEXPORT bool  World_hasComponent(World*, const eid&, const int);
HXAPI HXEXPORT void  World_addComponent(World*, const eid&, const int);
HXAPI HXEXPORT void  World_removeComponent(World*, const eid&, const int);
HXAPI HXEXPORT void  World_update(World*, int, Camera* cam);
HXAPI HXEXPORT void  World_draw(World*, Camera*);
HXAPI HXEXPORT void  World_cleanupExpiredEntities(World*);
HXAPI HXEXPORT bool  World_isSharedEdgeBetween(World*, const eid&, const eid&);
HXAPI HXEXPORT int   World_getNextAvailableEdgeFor(World*, const eid&);
HXAPI HXEXPORT float World_getDistanceBetween(World*, const eid&, const eid&);
HXAPI HXEXPORT bool  World_validateMove(World*, const eid&);//, const eid&);
HXAPI HXEXPORT eid   World_firstVisitedVertex(World*);
HXAPI HXEXPORT eid   World_currentVertex(World*);
HXAPI HXEXPORT int	 World_getNumberOfVertices(World*);
HXAPI HXEXPORT bool	 World_isLevelComplete(World*);
HXAPI HXEXPORT void  World_setPosition(World*, const eid&, float x, float y, float z);
HXAPI HXEXPORT void  World_resetPosition(World*, const eid&, float x, float y, float z);

HXAPI HXEXPORT void	 World_createEdge(World*, const eid&, const eid&, float);
HXAPI HXEXPORT void  World_createVertex(World*, float, float, float, const hxColor&);


HXAPI HXEXPORT void  World_easePositions(World* world, float t, int easingDelegateId);
HXAPI HXEXPORT void  World_ease(World* world, int dt);
HXAPI HXEXPORT void  World_updateTouches(World* world, Camera* cam);
HXAPI HXEXPORT void  World_physics(World* world, int dt);
HXAPI HXEXPORT void  World_renderVertices(World* world, Camera* cam);
HXAPI HXEXPORT void  World_renderEdges(World* world, Camera* cam) ;

#endif /* CLASSES_GAME_ECS_WORLD_H_ */
