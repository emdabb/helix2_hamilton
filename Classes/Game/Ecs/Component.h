/*
 * Components.h
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_ECS_COMPONENT_H_
#define CLASSES_GAME_ECS_COMPONENT_H_

struct Component {
	enum {
		Nil			= 0,
		Touchable	= 1 << 0,
		Transform	= 1 << 1,
		Visual		= 1 << 2,
		Physics		= 1 << 3,
		Constraint	= 1 << 4,
		Easing		= 1 << 5
	};
};



#endif /* CLASSES_GAME_ECS_COMPONENT_H_ */
