/*
 * Renderer.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>
#include <Classes/Game/Utility.h>
#include <Classes/Game/Camera.h>
#include <helix/engine/GameServices.h>
#include <helix/engine/GameArt.h>
#include <helix/core/Matrix.h>
#include <helix/gfx/Viewport.h>

using namespace helix::core;
using namespace helix::gfx;
using namespace helix::engine;

HXAPI Matrix createViewProjection(const Viewport&);

HXAPI HXEXPORT void World_renderVertices(World* world, Camera* cam) {
	ShaderProgram* shader 	= GameArt::getInstance().getShaderProgram("spriteBatchDefault");
	SpriteBatch* sprites 	= GameServices::getInstance().getSpriteBatch();
	Viewport viewport 		= GameServices::getInstance().getGraphicsDevice()->getViewport();

	Matrix viewProjection;// = createViewProjection(viewport);
	Matrix view, proj;
	cam->getTransform(&view);
	cam->getProjection(&proj);

	Matrix::mul(view, proj, &viewProjection);
    
    Color drawColor  = ColorPreset::White;

	sprites->begin(0, BlendState::AlphaBlend, Matrix::Identity, shader);
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, Component::Transform | Component::Visual)) {
			const transform_t& t = world->transform[i];
			const visual_t& v	 = world->visual[i];

			Vector3 projPos;
			Viewport::project(viewport, t.x, viewProjection, &projPos);

			Texture2D* tex = GameArt::getInstance().getTextureById(v.texRef);
            
            if((World_firstVisitedVertex(world) == i) || (World_currentVertex(world) == i) ){
                drawColor = v.color;
            } else {
                drawColor = ColorPreset::White;
            }

			sprites->draw(tex, (Vector2){ projPos.X, projPos.Y }, NULL,
					drawColor,
					0.0f, v.pivot, t.scale, 0, 1.0f);
		}
	}
	sprites->end();
}

HXAPI HXEXPORT void World_renderEdges(World* world, Camera* cam) {
	ShaderProgram* shader 	= GameArt::getInstance().getShaderProgram("spriteBatchDefault");
	SpriteBatch* sprites 	= GameServices::getInstance().getSpriteBatch();
	Viewport viewport 		= GameServices::getInstance().getGraphicsDevice()->getViewport();

	//Matrix viewProjection = createViewProjection(viewport);
	Matrix viewProjection;// = createViewProjection(viewport);
	Matrix view, proj;
	cam->getTransform(&view);
	cam->getProjection(&proj);

	Matrix::mul(view, proj, &viewProjection);

	sprites->begin(0, BlendState::AlphaBlend, Matrix::Identity, shader);
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, Component::Constraint)) {

			const constraint_t& c = world->constraint[i];
			const transform_t& tA = world->transform[c.a];
			const transform_t& tB = world->transform[c.b];

			Vector3 projPosA, projPosB;
			Viewport::project(viewport, tA.x, viewProjection, &projPosA);
			Viewport::project(viewport, tB.x, viewProjection, &projPosB);


			SpriteBatch_drawLine(
					sprites,
					(Vector2){projPosA.X, projPosA.Y},
					(Vector2){projPosB.X, projPosB.Y},
					ColorPreset::White,
					1.0f);

		}
	}
	sprites->end();
}

