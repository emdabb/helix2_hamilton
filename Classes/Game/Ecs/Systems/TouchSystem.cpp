/*
 * TouchSystem.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>
#include <Classes/Game/Input.h>
#include <Classes/Game/Camera.h>
#include <helix/core/Vector2.h>
#include <helix/gfx/Viewport.h>

using namespace helix::gfx;
using namespace helix::core;

//HXAPI HXEXPORT Matrix createViewProjection(const Viewport& vp) {
//	Matrix view, proj;
//	Matrix::createPerspectiveFov(MathHelper::TWO_OVER_PI, (float)vp.W / (float)vp.H, vp.MinDepth, vp.MaxDepth, &proj);
//	Matrix::createLookat(Vector3::Forward * 6.f, Vector3::Zero, Vector3::Up, &view);
//	return Matrix::mul(view, proj);
//}

HXAPI void World_updateTouches(World* world, Camera* cam) {

	Viewport vp;
	vp.X = vp.Y = 0;
	vp.W = 1024;
	vp.H = 768;
	vp.MinDepth = 0.1f;
	vp.MaxDepth = 100.f;

	Matrix viewProj, view, proj;// = createViewProjection(vp);
	cam->getTransform(&view);
	cam->getProjection(&proj);
	Matrix::mul(view, proj, &viewProj);

	Vector3 vPos;

	static bool wasDown = false;

	if(Input::MouseButtonDown[0]) {
		wasDown = true;
	} else {
		if(wasDown) {
			for(int i=0; i < _NUM_ENTITIES; i++) {
				if(World_hasComponent(world, i, Component::Touchable | Component::Transform)) {
					touchable_t& touch 	= world->touchable[i];
					transform_t& t		= world->transform[i];

					Viewport::project(vp, t.x, viewProj, &vPos);

					Vector2 vA = Input::MouseReleasePosition[0];
					Vector2 vB = { vPos.X, vPos.Y };

					float rr = (touch.Radius * touch.Radius) * t.scale;

					if(Vector2::lengthSq(vA - vB) <= rr) {
						TouchEventArgs args;
						args.world = world;
						args.id	   = i;
						touch.OnTouchBegin(args);
					}

				}
			}
			wasDown = false;
		}
	}


}


