/*
 * Easing.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>
#include <helix/core/Easing.h>
#include <helix/core/MathHelper.h>

using namespace helix::core;

HXAPI HXEXPORT void World_easePositions(World* world, float t, int easingDelegateId) {
	hxVector3 a = Vector3::Zero;
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, Component::Transform)) {
			hxVector3 b = world->transform[i].x;
			float c = Easing::Delegate[easingDelegateId](t);
			hxVector3::lerp(a, b, c, &world->transform[i].x);
		}
	}
}

HXAPI HXEXPORT void World_ease(World* world, int dt) {
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, Component::Easing)) {
			tween_t& t = world->tween[i];
			t.currentTime += dt * t.direction;
			t.currentTime = std::max(0, std::min(t.maxTime, t.currentTime));
			float a = t.startValue;
			float b = t.endValue;
			float c = (float)t.currentTime / (float)t.maxTime;
			float d = Easing::Delegate[t.tweenDelegateId](c);
			*t.valuePtr = MathHelper::lerp(a, b, d);
		}
	}
}



