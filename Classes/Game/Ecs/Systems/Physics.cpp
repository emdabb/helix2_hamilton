/*
 * Physics.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>

using namespace helix::core;

HXAPI void World_physics(World* world, int dt) {
	float fDt = (float)dt * 0.001f;
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, Component::Transform | Component::Physics)) {
			Vector3& a		= world->physics[i].a;
			Vector3& v 		= world->physics[i].v;
			Vector3& x		= world->transform[i].x;
			Vector3& oldx	= world->transform[i].oldx;
			Vector3 temp 	= x;
			v = oldx - x;
			x += v + a * fDt * fDt;
			oldx = temp;
			a = Vector3::Zero;
		}
	}
}

