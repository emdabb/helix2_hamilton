/*
 * Edge.cpp
 *
 *  Created on: Jul 28, 2015
 *      Author: miel
 */


#include <helix/Types.h>
#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>

HXAPI HXEXPORT eid new_Edge(World* world, int a, int b, float len) {
    eid id = World_createEntity(world);
    world->mask[id] = Component::Constraint;
    {
        constraint_t* c = &world->constraint[id];
        c->a = a;
        c->b = b;
        c->restLen = len;

        world->edges[a][World_getNextAvailableEdgeFor(world, a)] = id;
        world->edges[b][World_getNextAvailableEdgeFor(world, b)] = id;

    }
    return id;
}
