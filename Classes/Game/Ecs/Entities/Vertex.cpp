/*
 * Vertex.cpp
 *
 *  Created on: Jul 28, 2015
 *      Author: miel
 */

#include <helix/Types.h>
#include <helix/core/RandomNumber.h>
#include <helix/core/Easing.h>
//#include <helix/gfx/Color.h>
#include <helix/engine/GameArt.h>
#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>
#include <Classes/Game/Utility.h>

using namespace helix::core;
using namespace helix::gfx;
using namespace helix::engine;

HXAPI HXEXPORT void Vertex_onTouch(const TouchEventArgs& args) {
    World_validateMove(args.world, args.id);
}

HXAPI HXEXPORT eid new_Vertex(World* world, float x, float y, float z, const Color& color) {
	eid id = World_createEntity(world);

	World_addComponent(world, id, Component::Touchable | Component::Transform | Component::Visual | Component::Easing);

	transform_t& t  = world->transform[id];
	visual_t& v 	= world->visual[id];
	touchable_t& to = world->touchable[id];
	tween_t& tw		= world->tween[id];

	t.x.X 	= x;
	t.x.Y 	= y;
	t.x.Z 	= z;
	t.scale = 1.0f;

	int texId = GameArt::getInstance().getTextureId("@ball");
	Texture2D* tex = GameArt::getInstance().getTextureById(texId);

	v.color = color;
	v.pivot = (Vector2) {(float)tex->getWidth() * 0.5f, (float)tex->getHeight() * 0.5f };
	v.texRef= texId;

	to.Radius = (float)std::max(tex->getWidth(), tex->getHeight()) * 0.5f;
	to.OnTouchBegin += event(&Vertex_onTouch);

	tw.direction = -1;
	tw.currentTime = 0;
	tw.maxTime = 250;
	tw.startValue = 1.0f;
	tw.endValue = 2.0f;
	tw.tweenDelegateId = Easing::BackInOut;
	tw.valuePtr = &t.scale;


	return id;
}
