/*
 * Entity.h
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_ECS_ENTITY_H_
#define CLASSES_GAME_ECS_ENTITY_H_

#include <helix/Types.h>

#define _NUM_ENTITIES	512

typedef intptr_t eid;
typedef uint32_t bitmask_t;




#endif /* CLASSES_GAME_ECS_ENTITY_H_ */
