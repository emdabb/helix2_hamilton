/*
 * World.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <helix/engine/GameArt.h>
#include <helix/core/Easing.h>
#include <Classes/Game/Ecs/World.h>
#include <Classes/Game/Ecs/Component.h>
#include <cstring>

#define _MASK_VERTEX	(Component::Transform | Component::Easing | Component::Visual)

using namespace helix::engine;
using namespace helix::core;
using namespace helix::gfx;

class Camera;

HXAPI void Vertex_onTouch(const TouchEventArgs&);

HXAPI void  World_initialize(World* world) {
	memset(world->mask, Component::Nil, sizeof(bitmask_t) * _NUM_ENTITIES);
	memset(world->isExpired, 0, sizeof(bool) * _NUM_ENTITIES);
	memset(world->isVisited, 0, sizeof(bool) * _NUM_ENTITIES);
	memset(world->edges, -1, sizeof(edge_cache) * _NUM_ENTITIES);

	world->visited.clear();
	world->numMoves = 0;
	world->currentLevel = 0;
	world->OnLevelCompleted(0);
}
HXAPI void  World_destroy(World* world) {
	for(int i=0; i < _NUM_ENTITIES; i++) {
		World_destroyEntity(world, i);
	}
    world->visited.clear();
    world->numMoves = 0;
    memset(world->mask, Component::Nil, sizeof(bitmask_t) * _NUM_ENTITIES);
    memset(world->isExpired, 0, sizeof(bool) * _NUM_ENTITIES);
    memset(world->isVisited, 0, sizeof(bool) * _NUM_ENTITIES);
    memset(world->edges, -1, sizeof(edge_cache) * _NUM_ENTITIES);
    
    for(int i=0; i < _NUM_ENTITIES; i++) {
        world->touchable[i].OnTouchBegin.clear();
        world->touchable[i].OnTouchEnd.clear();
    }
    memset(world->tween, 0, sizeof(tween_t) * _NUM_ENTITIES);
}
HXAPI eid   World_createEntity(World* world) {
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(world->mask[i] == Component::Nil) {
			return i;
		}
	}
	return _NUM_ENTITIES;
}

HXAPI void  World_destroyEntity(World* world, const eid& id) {
	world->mask[id] 	 = Component::Nil;
	world->isExpired[id] = true;
}
HXAPI bool  World_hasComponent(World* world, const eid& id, const int type) {
	return ((world->mask[id] & type) == type);
}
HXAPI void  World_addComponent(World* world, const eid& id, const int type) {
	world->mask[id] |= type;
}

HXAPI void  World_removeComponent(World* world, const eid& id, const int type) {
	world->mask[id] &= ~type;
}
HXAPI void  World_update(World* world, int dt, Camera* cam) {
//	World_updatePositions(world, dt);
//	World_satisfyConstraints(world, dt, 3);
	World_updateTouches(world, cam);
	//World_easePositions(world, dt, )
	World_ease(world, dt);
}
HXAPI void  World_draw(World* world, Camera* cam) {
	World_renderEdges(world, cam);
	World_renderVertices(world, cam);

}
HXAPI void  World_cleanupExpiredEntities(World*) {

}
HXAPI void	World_createEdge(World* world, const eid& a, const eid& b, float len) {
	eid id = World_createEntity(world);
	World_addComponent(world, id, Component::Constraint);

	world->edges[a][World_getNextAvailableEdgeFor(world, a)] = id;
	world->edges[b][World_getNextAvailableEdgeFor(world, b)] = id;
}
HXAPI bool  World_isSharedEdgeBetween(World* world, const eid& a, const eid& b) {
	if(a == b) {
		return false;
	}
	for(int i=0; i < _MAX_DEGREE; i++) {
		eid edgeA = world->edges[a][i];

		for(int j=0; j < _MAX_DEGREE; j++) {
			eid edgeB = world->edges[b][j];

			if(edgeA == edgeB && (edgeA != -1) && (edgeB != -1)) {
				return true;
			}
		}
	}
	return false;
}
HXAPI int   World_getNextAvailableEdgeFor(World* world, const eid& id) {
	for(int i=0; i < _MAX_DEGREE; i++) {
		if(world->edges[id][i] < 0) {
			return i;
		}
	}
	return _MAX_DEGREE;
}
HXAPI void 	World_createVertex(World* world, float x, float y, float z, const hxColor& color) {
	eid id = World_createEntity(world);
	world->mask[id] = _MASK_VERTEX;
	transform_t& t  = world->transform[id];
	t.x.X 			= x;
	t.x.Y 			= y;
	t.x.Z 			= z;

	physics_t& p = world->physics[id];
	p.a			 = Vector3::Zero;
	p.v			 = Vector3::Zero;

	visual_t& v = world->visual[id];
	v.color		= color;
	v.texRef	= GameArt::getInstance().getTextureId("@ball");

	tween_t& tw = world->tween[id];

	tw.currentTime = 0;
	tw.maxTime = 500;
	tw.startValue = 0.0f;
	tw.endValue = 1.0f;
	tw.tweenDelegateId = Easing::BounceOut;
	tw.valuePtr = &t.scale;

	touchable_t& to = world->touchable[id];
	to.Radius = 10.0f;
	to.OnTouchBegin += event(&Vertex_onTouch);

}

HXAPI float World_getDistanceBetween(World* world, const eid& a, const eid& b) {
	const transform_t& tA = world->transform[a];
	const transform_t& tB = world->transform[b];
	return Vector3::length(tA.x - tB.x);
}

HXAPI bool  World_validateMove(World* world, const eid& a) { //, const eid& b) {
    bool res = false;
    
    if(world->visited.size()) {
        eid b = world->visited.back();
        if(a == b) {
            if(world->visited.size() != 1) {
                world->numMoves++;
                world->visited.pop_back();
                res = true;
            }
        } else {
            std::vector<eid>::iterator it = std::find(world->visited.begin(), world->visited.end(), a);
            if(it == world->visited.end()) {
                if(World_isSharedEdgeBetween(world, a, b)) {
                    world->visited.push_back(a);
                    world->numMoves++;
                    res = true;
                }
            }
        }
        
    } else {
        world->visited.push_back(a);
        world->numMoves++;
        res = true;
    }
    
    


    if(res != false) {
		world->tween[a].direction = world->tween[a].direction * -1;
	}

	if(World_isLevelComplete(world)) {
		world->OnLevelCompleted(++world->currentLevel);
	}
	return res;
}

HXAPI eid   World_firstVisitedVertex(World* world) {
	return world->visited.front();
}
HXAPI eid   World_currentVertex(World* world) {
	return world->visited.back();
}

HXAPI int	 World_getNumberOfVertices(World* world) {
	int count = 0;
	for(int i=0; i < _NUM_ENTITIES; i++) {
		if(World_hasComponent(world, i, _MASK_VERTEX)) {
			count++;
		}
	}
	return count;
}

HXAPI bool	 World_isLevelComplete(World* world) {
	int count 	= World_getNumberOfVertices(world);
	int n 		= (int)world->visited.size();
	return (count == n) && World_isSharedEdgeBetween(world, World_currentVertex(world), World_firstVisitedVertex(world));
}

HXAPI void  World_setPosition(World* world, const eid& id, float x, float y, float z) {

		world->transform[id].x.X = x;
		world->transform[id].x.Y = y;
		world->transform[id].x.Z = z;

}

HXAPI void  World_resetPosition(World* world, const eid& id, float x, float y, float z) {

			world->transform[id].x.X 	= x;
			world->transform[id].x.Y 	= y;
			world->transform[id].x.Z 	= z;
			world->transform[id].oldx.X = x;
			world->transform[id].oldx.Y = y;
			world->transform[id].oldx.Z = z;

}
